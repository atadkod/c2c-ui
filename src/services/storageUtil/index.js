const setToken = (details) => {
    sessionStorage.setItem("Token", details);
};

const getToken = () => {
    return sessionStorage.getItem("Token");
};
const deleteToken = () => {
    sessionStorage.removeItem("Token")
}
export { setToken, getToken, deleteToken };
