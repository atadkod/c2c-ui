import React, { useEffect, useState } from 'react';
import { getAllocatedBids } from '../../../services/allocations';
import { getAppliedBids, getHighestBid } from '../../../services/Bid';
import { getCategoryById } from '../../../services/category.js';
import { getProductDetails, getProductDetailsWithoutAvailability } from '../../../services/products';
import Card from './Card';


const AllocatedBids = () => {
    const [productList, setProductList] = useState([])
    useEffect(() => {
        (async () => {
            const res = await getAllocatedBids();
            console.log(res.data)
            const newRes = []
            if (res.data) {
                for (const r of res.data) {
                    const prod = await getProductDetailsWithoutAvailability(r.productId)
                    const cat = await getCategoryById(prod.data.categoryId)
                    newRes.push({ ...r, productName: prod.data.name, category: cat.data.name })
                }
                setProductList(newRes)
            }
        })()
    }, [])
    return (productList ?
        <div>
            {productList.map(product => {
                return < Card product={product} />
            })
            }
        </div> : null
    );
};

export default AllocatedBids;