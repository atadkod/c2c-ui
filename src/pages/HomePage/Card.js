import React, { useEffect, useState } from 'react';
import './homepage.css'
import BidPrice from '../Product/BidPrice';
import Modal from '../../components/Modal';
import { useNavigate } from 'react-router';


const Card = (props) => {
    const [product, setProduct] = useState(null)
    const navigate = useNavigate();
    const [closeBtn, setCloseBtn] = useState(null)
    useEffect(() => {
        setProduct(props.product)
    }, [props.product])
    const goToProductPage = () => {
        navigate(`/product/${props.product.uuid}`)
    }

    const watchlisting = <><div>Do you want to add this product to your watchlist!!!</div>
        <br />
        <div onClick={() => { console.log("hi") }}>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div></>

    return (
        <div className="container">
            <div class="card shadow p-3 mb-5 bg-body rounded" >
                {product ? <>
                    <div class="card-body">
                        <div style={{ justifyContent: "center", display: "flex" }} style={{ textAlign: "center" }}>
                            <img className="img-fluid" src={"Product1.png"} style={{ height: "200px" }} onClick={goToProductPage} />
                        </div>
                        <p class="card-text" style={{ textAlign: "center" }} onClick={goToProductPage}>
                            <h4 className="text-center">{product ? props.product.name : ""}</h4>

                            <h4 className="text-center"><span className="text-success">{product ? props.product.price : ""}$</span><br /><span style={{ fontSize: "12px" }}>Product Base Price</span></h4>
                        </p>

                        <div>
                            <button type="button" class="btn btn-dark">
                                <a id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" data-bs-toggle="modal" data-bs-target={`#bid${product.uuid}`} > Bid an amount</a>
                            </button>
                            &nbsp;&nbsp;&nbsp;
                            <button type="button" class="btn btn-dark">
                                <a id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" data-bs-toggle="modal" data-bs-target={`#watchlist${product.uuid}`}>
                                    Add to watchlist
                                </a>
                            </button>
                        </div>

                    </div>
                    <Modal modalName={`bid${product.uuid}`} setCloseBtn={setCloseBtn} title="Submit your bid" content={<BidPrice closeButton={closeBtn} product={product ? product : null} />} />

                    <Modal modalName={`watchlist${product.uuid}`} title="WatchList the product" content={watchlisting} /> </> : null}


            </div>
        </div>
    );
};

export default Card;