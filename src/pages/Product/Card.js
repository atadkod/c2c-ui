import React from 'react';

function Card(props) {
    
    return (
        <div>
             <div class="card text-dark m-4" style={{borderRadius:"15px"}}>
              <div class="card-body p-5">
                  <div className="row">


                <div className="">
                    <div class="">
                       
                      
                      <span className="fs-1">Brand Name:</span>
                      <span className="fw-bold display-4" style={{}}>{props.product.brandName}</span>
                    </div>
                </div>
                

                <div className="">
                    <div class="">
                      
                       
                       Starting bid:
                       <span className="fw-bold text-success" style={{fontSize:"30px"}}>{props.product.price}$</span>
                    </div>
                </div>

                
                <div className="">
                <div class="">
                       <span className="fw-normal" style={{fontSize:"15px"}}>Bid Amount:</span>
                       
                       <span className="fw-normal text-success" style={{fontSize:"30px"}}>150$</span>
                    </div>
                </div>
                <div className="">
                <div class="">
                       <span className="fw-normal" style={{fontSize:"15px"}}>Highest:</span>
                      
                       <span className="fw-normal text-success" style={{fontSize:"30px"}}>150$</span>
                    </div>
                </div>
               
                <div className="">
                <br/><br/>
                    <button type="button" class="btn btn-dark">
                        
                       <a id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" data-bs-toggle="modal" data-bs-target="#bid"> Bid an amount</a>
                    </button>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <button type="button" class="btn btn-dark">
                    <a id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" data-bs-toggle="modal" data-bs-target="#watchlist">
                        Add to WatchList
                    </a>
                    </button>
                </div>

                <div className="col-md-2">
                </div>
                </div>
                
              </div>
            </div>
        </div>
    );
}

export default Card;