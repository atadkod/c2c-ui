import React, { Component } from 'react'

export class Confirm extends Component {
    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    };

    back = e => {
        e.preventDefault();
        this.props.prevStep();
    };

    render() {
        const {
            values: { title, category, subcategory, condition, itemdescription, images, startdate, enddate, price, quantity, paymentmethod }
        } = this.props;

        return (
            <div className="form-container">
                <h1 className="mb-5">Confirm</h1>
                <ul class="list-group">
                    <li class="list-group-item">Title: {title}</li>
                    <li class="list-group-item">Category: {category}</li>
                    <li class="list-group-item">Sub Category: {subcategory}</li>
                    <li class="list-group-item">Condition: {condition}</li>
                    <li class="list-group-item">Item Description: {itemdescription}</li>
                    <li class="list-group-item">Start Date: {startdate}</li>
                    <li class="list-group-item">End Date: {enddate}</li>
                    <li class="list-group-item">Price: {price}</li>
                    <li class="list-group-item">Quantity: {quantity}</li>
                    <li class="list-group-item">Payment Method: Cash on delivery</li>
                </ul>

                <br /><br />

                <div className="row">
                    <div className="col-6">
                        <button className="btn btn-danger" onClick={this.back}>Back</button>
                    </div>
                    <div className="col-6 text-right">
                        <button className="btn btn-primary" onClick={this.props.postData}>Continue</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default Confirm