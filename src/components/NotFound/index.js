import React from "react";
import "./style.css";

// const baseUrl = window.location.origin;
// const redirectUrl = `${baseUrl}/customer`;

const NotFound = () => (
  <section className="not-found-section">
    <p className="not-found-title">404: PAGE NOT FOUND</p>
    <p className="not-found-subtitle">
      THE PAGE YOU WERE LOOKING FOR DOESN'T EXIST.
    </p>
    <p className="not-found-message">
      YOU MAY HAVE MISTYPED THE ADDRESS OR PAGE MAY HAVE MOVED.
    </p>
  </section>
);

export default NotFound;
