import axios from "axios";
import { getToken } from "../storageUtil";

const config = {
    headers: {
        "Cache-Control": "no-cache",
        "Content-Type": "application/json",
        "x-access-token": getToken()
    },
};

export const API = axios.create(config);