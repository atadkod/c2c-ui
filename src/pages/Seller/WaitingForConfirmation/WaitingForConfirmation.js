import { useEffect, useState } from "react";
import { getProductsWaitingForConfirm } from "../../../services/allocations";
import { getHighestBid } from "../../../services/Bid";
import { getProductDetailsWithoutAvailability } from "../../../services/products";
import { getUserById } from "../../../services/user";
import Card from "./Card";

const WaitingForConfirmation = () => {
    const [productList, setProductList] = useState([])

    useEffect(() => {
        (async () => {
            const res = await getProductsWaitingForConfirm();
            console.log(res)
            const newRes = []
            console.log(res)
            if (res.data) {
                for (const r of res.data) {
                    const prod = await getProductDetailsWithoutAvailability(r.productId)
                    const user = await getUserById(r.buyerId)
                    console.log(user.data)
                    newRes.push({ ...r, product: prod.data, user: user.data })
                }
                setProductList(newRes)
            }
        })()
    }, [])
    return (productList ?
        <div>
            {productList.map(product => {
                return < Card product={product} />
            })
            }
        </div> : null
    );
};

export default WaitingForConfirmation;