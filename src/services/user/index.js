import { API } from "../httpHandler/index";
import { getToken, setToken } from "../storageUtil";

export const userLogin = async (body) => {
    try {
        const data = await API.post(
            "http://localhost:5050/user/login", body
        );
        setToken(data.data.token)
        console.log(data)
        return {
            data: data.data,
        };
    } catch (err) {
        return {
            error: err
        }
    }
}

export const getUserById = async (id) => {
    try {
        const data = await API.get(
            "http://localhost:5050/user", { params: { id } }
        );
        return {
            data: data.data,
        };
    } catch (err) {
        return {
            error: err
        }
    }
}