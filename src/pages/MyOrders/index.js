import React from 'react';
import { getToken } from '../../services/storageUtil';
import Navbar from '../HomePage/NavBar';
import '../Customer/index.css';
import AllOrders from './AllOrders';
import SoldProducts from './SoldProducts';
import PurchasedProducts from './PurchasedProducts';



const MyOrders = () => {
    const t = getToken()
    if (t == null) {
        window.location.href = "/"
    }
    return (<>
        {t ? (<div>
            < Navbar />
            <div className="container">

                <div class="m-4">
                    <ul class="nav nav-tabs text-center display-3 fw-bold" id="myTab" style={{ 'margin-top': '20px', display: 'flex', justifyContent: 'center' }}>
                        <li class="nav-item">
                            <a href="#home" class="nav-link active" data-bs-toggle="tab">All orders</a>
                        </li>
                        <li class="nav-item">
                            <a href="#profile" class="nav-link" data-bs-toggle="tab">Sold Products</a>
                        </li>
                        <li class="nav-item">
                            <a href="#messages" class="nav-link" data-bs-toggle="tab">Purchased Products</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="home">
                            <AllOrders/>
                        </div>
                        <div class="tab-pane fade" id="profile">
                            <SoldProducts />
                        </div>
                        <div class="tab-pane fade" id="messages">
                             <PurchasedProducts/>
                        </div>
                    </div>
                </div>
            </div>
        </div >) : null}</>
    );
};

export default MyOrders;