import React, { useEffect } from 'react';
import './BidForm.css';
import Form from '../../components/Form';

function BidForm() {
  return (
    <div className="BidForm">
      <div className="container">
        <Form />
      </div>
    </div>
  );
}

export default BidForm;