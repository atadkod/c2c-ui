
import Navbar from "./NavBar";
import ProductsView from "./ProductsView/ProductsView";

function Homepage() {
  return (
    <div className="App">
      <div className="container-fluid" style={{ height: "4vh", backgroundColor: "black" }}></div>

      <Navbar />

      <div className="" style={{ backgroundImage: "url(Mainpage.png)", height: "70vh", backgroundSize: "cover" }}></div>

      <div className="container-fluid" style={{ height: "10vh", backgroundColor: "black" }}></div>

      <ProductsView />


    </div>
  );
}

export default Homepage;
