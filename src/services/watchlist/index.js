import { API } from "../httpHandler/index";
import { getToken, setToken } from "../storageUtil";

export const getWishListByBuyerId = async () => {
    try {
        const data = await API.get(
            "http://localhost:5050/watchlist/byBuyerId"
        );
        console.log(data)
        return {
            data: data.data,
        };
    } catch (err) {
        return {
            error: err
        }
    }
}

export const postWatchList = async (body) => {
    try {
        const data = await API.post(
            "http://localhost:5050/watchlist/", body
        );
        console.log(data)
        return {
            data: data.data,
        };
    } catch (err) {
        return {
            error: err
        }
    }
}
