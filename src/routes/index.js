import React from "react";
import { Routes, Route, Outlet } from "react-router-dom"

import NotFound from "../components/NotFound";
import Customer from "../pages/Customer";
import HomePage from "../pages/HomePage/HomePage";
import BidForm from "../pages/BidForm/BidForm";
import Registration from "../pages/HomePage/Registration/Registration";
import Product from "../pages/Product/Product";
import SellerProduct from "../pages/SellerProduct";
import Seller from "../pages/Seller";
import MyOrders from "../pages/MyOrders";


export default function AppRoutes() {
  return (
    <Routes>
      <Route path="/" element={<HomePage />} />
      <Route path="*" element={<NotFound />} />
      <Route path="/product/:id" element={<Product />} />
      <Route path="/sellerProduct/:id" element={<SellerProduct />} />
      <Route path="/customer" element={<Customer />} />
      <Route path="/bidForm" element={<BidForm />} />
      <Route path="/registration" element={<Registration />} />
      <Route path="/seller" element={<Seller />} />
      <Route path="/myorders" element={<MyOrders />} />
    </Routes>
  );
}
