import React from "react";
import { useForm } from "react-hook-form";
import { userLogin } from "../../services/user";
import { useLocation, useNavigate } from 'react-router-dom'
import './homepage.css'

export default function Login({ closeButton }) {
    const { register, handleSubmit } = useForm();
    const navigate = useNavigate();
    const location = useLocation();
    const onSubmit = async (data) => {
        const result = await userLogin(data)
        if (result.error != null) {
            console.log(result.error)
        } else {
            closeButton.click()
            window.location.href = "/"
        }

    };

    return (
        <form onSubmit={handleSubmit(onSubmit)}>

            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label" >Email address</label>
                <input type="email" class="form-control" {...register("email")} id="exampleInputEmail1" aria-describedby="emailHelp" />
                <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label" >Password</label>
                <input type="password" class="form-control" {...register("password")} id="exampleInputPassword1" />
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>

        </form >


    );
}