import React, { useEffect, useState } from 'react';
import { getCategory } from '../../services/category.js';
import './homepage.css'

const Categories =  (props) => {
    const [cat,setCat] = useState([])

    useEffect(async()=>{
    const categories = await getCategory();
    let cArray = []
    console.log(categories.data)
    if(categories.data){
        categories.data.forEach((c) => {
            cArray.push(c.name)
            console.log(c.name)
        });
        setCat(cArray)
    }
    },[])

    return (
        
        <div className="container p-5">
            <div className="fs-1 text-center">Categories</div>
            <div className="row p-5">
                
                {cat.map((c)=>
                    <div className="col-md-2 shadow p-3 mr-5 mb-3 rounded ms-3 hoverNav" style={{ backgroundColor: "#dbdfef" }}>
                    <div>
                        <span className="fw-bolder" onClick={()=>props.handleCategory(c)} >{c}</span><br />
                    </div>
                </div>
                )}
            </div>
        </div>
    );
};

export default Categories;