import { API } from "../httpHandler";

export const getAllocatedBids = async () => {
    try {
        const data = await API.get(
            "http://localhost:5050/allocated/buyer"
        );
        return {
            data: data.data,
        };
    } catch (err) {
        return {
            error: err
        }
    }
}

export const confirmFromBuyer = async (body) => {
    console.log(body)
    const newBody = {
        bidId: body.bidId,
        productId: body.productId,
        buyerId: body.buyerId,
        sellerId: body.sellerId,
        price: body.bidPrice
    }

    try {
        const data = await API.post(
            "http://localhost:5050/bidding/buyconfirmed", newBody
        );
        return {
            data: data.data,
        };
    } catch (err) {
        return {
            error: err
        }
    }
}

export const cancelFromBuyer = async (body) => {
    console.log(body)
    const newBody = {
        bidId: body.bidId,
        productId: body.productId,
        buyerId: body.buyerId,
        sellerId: body.sellerId,
        price: body.price
    }
    try {
        const data = await API.post(
            "http://localhost:5050/bidding/buydenied", newBody
        );
        return {
            data: data.data,
        };
    } catch (err) {
        return {
            error: err
        }
    }
}

export const getProductsWaitingForConfirm = async () => {

    try {
        const data = await API.get(
            "http://localhost:5050/allocated/seller"
        );
        return {
            data: data.data,
        };
    } catch (err) {
        return {
            error: err
        }
    }
}