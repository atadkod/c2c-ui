import React from 'react';

function Card({ product }) {
    return (
        <div>
            {product ? <div class="card text-dark bg-light m-5" style={{ borderRadius: "15px" }}>
                <div class="card-body p-5">
                    <div className="row">
                        <div className="col-md-3">
                            <div class="" style={{ margin: '0', position: 'absolute', top: '50%', msTransform: 'translateY(-50%)', transform: 'translateY(-50%)' }}>
                                <span className="fw-bold" style={{ fontSize: "30px" }}>{product.product.name}</span>
                                <br />

                            </div>
                        </div>

                        <div className="col-md-2">
                            <div class="">
                                <span className="fw-bold text-success" style={{ fontSize: "30px" }}>${product.product.price}</span>
                                <br />
                                Starting bid
                            </div>
                        </div>
                        <div className="col-md-2">
                            <div class="">
                                <span className="fw-bold text-success" style={{ fontSize: "30px" }}>${product.price}</span>
                                <br />
                                Bid Amount
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div class="">
                                <span className="fw-bold text-success" style={{ fontSize: "30px" }}>{`${product.user.firstname} ${product.user.lastname}`}</span>
                                <br />
                                Buyer
                            </div>
                        </div>
                    </div>
                </div>
            </div> : null
            }
        </div>
    );
}

export default Card;