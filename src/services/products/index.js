import { getSubCategoryByName } from "../category.js";
import { API } from "../httpHandler";


export const postProduct = async (body) => {
    const cat = await getSubCategoryByName(body.category)
    const newBody = {
        name: body.title,
        categoryId: cat.data[0].id,
        description: body.itemdescription,
        price: body.price,
        brandName: "default",
        availability: true,
        startTime: body.startdate,
        endTime: body.enddate
    }
    try {
        const data = await API.post(
            "http://localhost:5050/product", newBody
        );
        return {
            data: data.data,
        };
    } catch (err) {
        return {
            error: err
        }
    }
}

export const getProductDetails = async (id) => {
    try {
        const data = await API.get(
            "http://localhost:5050/product", { params: { id } }
        );
        return {
            data: data.data,
        };
    } catch (err) {
        return {
            error: err
        }
    }
}

export const getProductDetailsWithoutAvailability = async (id) => {
    try {
        const data = await API.get(
            "http://localhost:5050/product/allocated", { params: { id } }
        );
        return {
            data: data.data,
        };
    } catch (err) {
        return {
            error: err
        }
    }
}

export const getAllProducts = async () => {
    try {
        const data = await API.get(
            "http://localhost:5050/product/all"
        );
        console.log("****" + JSON.stringify(data.data))
        return {
            data: data.data,
        };
    } catch (err) {
        return {
            error: err
        }
    }
}

export const getProuctsForBiding = async () => {
    try {
        const data = await API.get(
            "http://localhost:5050/product/seller"
        );
        console.log("****" + JSON.stringify(data.data))
        return {
            data: data.data,
        };
    } catch (err) {
        return {
            error: err
        }
    }
}