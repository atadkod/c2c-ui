import React from 'react';
import Navbar from '../HomePage/NavBar';
import '../Customer/index.css';
import ProductsOutForBiding from './ProductsOutForBiding';
import WaitingForConfirmation from './WaitingForConfirmation/WaitingForConfirmation'

const Seller = () => {
    return (
        <div>
            <Navbar />
            <div className="container">

                <div class="m-8">
                    <ul class="nav nav-tabs text-center display-2 fw-bold" style={{ fontSize: '50px', 'margin-top': '20px', display: 'flex', justifyContent: 'center' }} id="myTab">
                        <li class="nav-item">
                            <a href="#home" class="nav-link active" data-bs-toggle="tab">Products out for bidding</a>
                        </li>
                        <li class="nav-item">
                            <a href="#profile" class="nav-link" data-bs-toggle="tab">Waiting for confirmation</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="home">
                            <ProductsOutForBiding />
                        </div>
                        <div class="tab-pane fade" id="profile">
                            <WaitingForConfirmation />
                        </div>
                    </div>
                </div>
            </div>
        </div >
    );
};

export default Seller;