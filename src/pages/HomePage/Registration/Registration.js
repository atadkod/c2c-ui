import React from "react";
import { useForm } from "react-hook-form";


function Registration() {

    const { register, handleSubmit } = useForm();
    const onSubmit = data => console.log(data);

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
        <section class="vh-100">
        <div class="mask d-flex align-items-center">
          <div class="container h-100">
            <div class="row d-flex justify-content-center align-items-center h-100">
              <div class="col-12 col-md-9 col-lg-7 col-xl-6">
                <div class="card">
                  <div class="card-body p-5">
                    <h2 class="text-uppercase text-center mb-5">Create an account</h2>
      
                    <form>
      
                      <div class="form-outline mb-4">
                        <label class="form-label" for="form3Example1cg">First Name</label>
                        <input type="text" id="firstName" class="form-control form-control-lg" />
                      </div>

                      <div class="form-outline mb-4">
                        <label class="form-label" for="form3Example1cg">Last Name</label>
                        <input type="text" id="lastName" class="form-control form-control-lg" />
                      </div>
      
                      <div class="form-outline mb-4">
                      <label class="form-label" for="form3Example3cg">Your Email</label>
                        <input type="email" id="email" class="form-control form-control-lg" />
                      </div>

                      <div class="form-outline mb-4">
                        <label class="form-label" for="form3Example8">Address</label>
                        <input type="text" id="address" class="form-control form-control-lg" />
                      </div>

                      <div class="row">
                    <div class="col-md-2 mb-4 pb-2">

                      <div class="form-outline form-white">
                      <label class="form-label" for="form3Examplea7">Code +</label>
                        <input type="text" id="code" class="form-control form-control-lg" />
                      </div>

                    </div>
                    <div class="col-md-10 mb-4 pb-2">

                      <div class="form-outline form-white">
                      <label class="form-label" for="form3Examplea8">Phone Number</label>
                        <input type="text" id="phoneNumber" class="form-control form-control-lg" />
                      </div>

                    </div>
                  </div>
      
                      <div class="form-outline mb-4">
                      <label class="form-label" for="form3Example4cg">Password</label>
                        <input type="password" id="password" class="form-control form-control-lg" />
                      </div>
      
                      <div class="d-flex justify-content-center">
                        <button type="submit" class="btn btn-primary btn-xl" >Register</button>
                      </div>
      
                      <p class="text-center text-muted mt-5 mb-0">Have already an account? <a href="#!" class="fw-bold text-body"><u>Login here</u></a></p>
      
                    </form>
      
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      </form>

    
  );
}

export default Registration;