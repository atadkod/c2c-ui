import React, { useEffect, useState } from "react";
import { getHighestBid } from "../../services/Bid";
import { getProductDetails, getProuctsForBiding } from "../../services/products";
import Card from "./Card";

const ProductsOutForBiding = () => {
    const [productList, setProductList] = useState([])

    useEffect(() => {
        (async () => {
            const res = await getProuctsForBiding();
            const newRes = []
            // console.log("1234" + JSON.stringify(res.data))
            if (res.data) {
                for (const r of res.data) {
                    const highest = await getHighestBid(r.uuid)
                    console.log(highest)
                    newRes.push({ ...r, highestBidder: highest.data })
                }
                setProductList(newRes)
            }
        })()
    }, [])
    return (productList ?
        <div>
            {productList.map(product => {
                return < Card product={product} />
            })
            }
        </div> : null
    );
};

export default ProductsOutForBiding;