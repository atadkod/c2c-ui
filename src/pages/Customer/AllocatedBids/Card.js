import React from 'react';
import { useNavigate } from 'react-router';
import { cancelFromBuyer, confirmFromBuyer } from '../../../services/allocations';

function Card({ product }) {
    const navigate = useNavigate()
    // console.log(product)
    const handleConfirm = async () => {
        const res = await confirmFromBuyer(product);
        navigate("/")
    }
    const handleCancel = async () => {
        const res = await cancelFromBuyer(product);
        navigate("/")
    }
    return (
        <div>
            {product ? <div class="card text-dark bg-light m-5" style={{ borderRadius: "15px" }}>
                <div class="card-body p-5">
                    <div className="row">
                        <div className="col-md-3">
                            <div class="" style={{ margin: '0', position: 'absolute', top: '50%', msTransform: 'translateY(-50%)', transform: 'translateY(-50%)' }}>
                                <span className="fw-bold" style={{ fontSize: "30px" }}>{product.productName}</span>
                                <br />

                            </div>
                        </div>

                        <div className="col-md-2">
                            <div class="">
                                <span className="fw-bold text-success" style={{ fontSize: "30px" }}>${product.category}</span>
                                <br />
                                Category
                            </div>
                        </div>
                        <div className="col-md-2">
                            <div class="">
                                <span className="fw-bold text-success" style={{ fontSize: "30px" }}>{product.price}$</span>
                                <br />
                                Price at which you bided the product
                            </div>
                        </div>

                        <div className="col-md-2">
                            <button onClick={handleConfirm} type="button" class="btn btn-dark" style={{ margin: '0', position: 'absolute', top: '50%', msTransform: 'translateY(-50%)', transform: 'translateY(-50%)' }}>Confirm</button>
                        </div>
                        <div className="col-md-2">
                            <button onClick={handleCancel} type="button" class="btn btn-dark" style={{ margin: '0', position: 'absolute', top: '50%', msTransform: 'translateY(-50%)', transform: 'translateY(-50%)' }}>Cancel</button>
                        </div>
                    </div>
                </div>
            </div> : null
            }
        </div>
    );
}

export default Card;