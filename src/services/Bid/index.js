import { API } from "../httpHandler/index";
import { getToken, setToken } from "../storageUtil";

export const bidPrice = async (body, product) => {
    try {
        const newBody = {
            productId: product.uuid,
            sellerId: product.sellerId,
            basePrice: product.price,
            bidPrice: body.price,
            message: body.message,
            isActive: true
        }
        console.log(newBody)
        const data = await API.post(
            "http://localhost:5050/bidding", newBody
        );
        return {
            data: data.data,
        };
    } catch (err) {
        return {
            error: err
        }
    }
}

export const getAppliedBids = async () => {
    try {
        const data = await API.get(
            "http://localhost:5050/bidding/buyer"
        );
        return {
            data: data.data,
        };
    } catch (err) {
        return {
            error: err
        }
    }
}
export const getBidsByProductId = async (id) => {
    try {
        const bids = await API.get(
            "http://localhost:5050/bidding/product", { params: { id } }
        );
        return {
            data: bids.data
        };
    } catch (err) {
        return {
            error: err
        }
    }
}


export const getHighestBid = async (id) => {
    try {
        const bids = await API.get(
            "http://localhost:5050/bidding/product/highest", { params: { id } }
        );
        return {
            data: bids.data
        };
    } catch (err) {
        return {
            error: err
        }
    }
}

export const chooseBid = async (body) => {
    const newBody = {
        bidId: body.bidId,
        productId: body.productId,
        buyerId: body.buyerId,
        sellerId: body.sellerId,
        price: body.bidPrice
    }
    console.log(newBody)
    try {
        const data = await API.post(
            "http://localhost:5050/bidding/sellconfirmed", newBody
        );
        return {
            data: data.data,
        };
    } catch (err) {
        return {
            error: err
        }
    }
}
