import React from 'react';
import { getToken } from '../../services/storageUtil';
import Navbar from '../HomePage/NavBar';
import AppliedBids from './AppliedBids';
import './index.css';
import AllocatedBids from './AllocatedBids/AllocatedBids'
import WatchList from './WatchList/WatchList';

const Customer = () => {
    const t = getToken()
    console.log(t)
    if (t == null) {
        window.location.href = "/"
    }
    return (<>
        {t ? (<div>
            < Navbar />
            <div className="container">

                <div class="m-4">
                    <ul class="nav nav-tabs text-center display-3 fw-bold" id="myTab" style={{ 'margin-top': '20px', display: 'flex', justifyContent: 'center' }}>
                        <li class="nav-item">
                            <a href="#home" class="nav-link active" data-bs-toggle="tab">Applied Bids</a>
                        </li>
                        <li class="nav-item">
                            <a href="#profile" class="nav-link" data-bs-toggle="tab">Allocated Bids</a>
                        </li>
                        <li class="nav-item">
                            <a href="#messages" class="nav-link" data-bs-toggle="tab">WatchList</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="home">
                            <AppliedBids />
                        </div>
                        <div class="tab-pane fade" id="profile">
                            <AllocatedBids />
                        </div>
                        <div class="tab-pane fade" id="messages">
                            <WatchList />
                        </div>
                    </div>
                </div>
            </div>
        </div >) : null}</>
    );
};

export default Customer;