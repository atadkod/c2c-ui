import React from 'react';
import { useNavigate } from 'react-router';

function Card({ product }) {
    const navigate = useNavigate()
    const handleProduct=()=>{
        navigate("/sellerProduct/"+product.uuid,{state:product})
    }
    return (
        <div>
            {product ? <div class="card text-dark bg-light m-4" style={{ borderRadius: "15px", 'maxWidth': "800px", left: "20%" }}>
                <div class="card-body p-4" >
                    <div className="row" >
                        <div className="col-md-3">
                            <div class="" style={{ margin: '0', position: 'absolute', top: '50%', msTransform: 'translateY(-50%)', transform: 'translateY(-50%)' }}>
                                <span className="fw-bold" style={{ fontSize: "30px" }}>{product.name}</span>
                                <br />

                            </div>
                        </div>

                        <div className="col-md-3">
                            <div class="">
                                <span className="fw-bold text-success" style={{ fontSize: "30px" }}>${product.price}</span>
                                <br />
                                Starting bid
                            </div>
                        </div>

                        <div className="col-md-3">
                            <div class="">
                                <span className="fw-bold text-success" style={{ fontSize: "30px" }}>{product.highestBidder ? `${product.highestBidder.bidPrice}$` : "No Bid"}</span>
                                <br />
                                Highest Bid
                            </div>
                        </div>

                        <div className="col-md-2">
                            <button type="button" class="btn btn-dark" onClick={handleProduct} style={{ margin: '0', position: 'absolute', top: '50%', msTransform: 'translateY(-50%)', transform: 'translateY(-50%)' }}>Click to view</button>
                        </div>
                    </div>
                </div>
            </div> : null
            }
        </div >
    );
}

export default Card;