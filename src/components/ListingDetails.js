import React, { Component, useEffect, useState } from 'react'
import { getCategory, getSubCategory } from "../services/category.js"
const ListingDetails = (props) => {
    const [category, setCategory] = useState([]);
    const [subCategory, setSubCategory] = useState([]);
    const handleNext = e => {
        e.preventDefault();
        props.nextStep();
    };
    useEffect(() => {
        (async () => {
            const res = await getCategory();
            console.log("###" + JSON.stringify(res.data))
            if(res.data){
                setCategory(res.data)
                values.category = res.data[0].name
                const res2 = await getSubCategory(res.data[0].id);
    
                console.log("*******" + JSON.stringify(res2.data))
                setSubCategory(res2.data)
                values.subCategory = res2.data[0].name
                const eve = new CustomEvent('category')
                Object.defineProperty(eve, 'target', { writable: false, value: { value: res.data[0].name, name: 'category' } })
                // eve.target.value = res.data[0].name
                // eve.target.name = 'category'
                handleChange(eve)
                const eve2 = new CustomEvent('subCctegory')
                Object.defineProperty(eve2, 'target', { writable: false, value: { value: res2.data[0].name, name: 'subcategory' } })
                handleChange(eve2)
            }
        })()
    }, [])
    const { values, inputChange } = props;

    const [imageList, setImageList] = useState([]);
    const handleImageChange = (e) => {
        console.log(imageList)
        const filesUrl = []
        setImageList([...imageList, URL.createObjectURL(e.target.files[0])])
        inputChange(e.target.name, e)
    }

    const handleCatChange = async (e) => {
        inputChange(e.target.name, e)
        const id = category.filter(c => c.name == e.target.value)
        // console.log(id)
        const res2 = await getSubCategory(id[0].id);
        // console.log("*******" + JSON.stringify(res2.data))
        // console.log(e)
        setSubCategory(res2.data)
        const eve2 = new Event('subcategory')
        Object.defineProperty(eve2, 'target', { writable: false, value: { value: res2.data[0].name, name: 'subcategory' } })
        handleChange(eve2)

    }
    const handleChange = (e) => {
        inputChange(e.target.name, e)
    }
    return (
        <div className="form-container">
            <h1 className="mb-5">Listing details</h1>
            <div className="form-group">
                <label htmlFor="title">Title</label>
                <input type="text" className="form-control" id="title" name="title" onChange={handleChange} value={values.title} />
            </div>
            <br />
            <div class="form-group">
                <label htmlFor="category">Category</label>
                <select class="form-control" id="category" name="category" onChange={handleCatChange} value={values.category}>
                    {
                        category?
                        category.map((cat, index) => {
                            return <option >{cat.name}</option>
                        }):<option >Others</option>
                    }
                </select>
            </div>
            <br />
            <div class="form-group">
                <label htmlFor="subcategory">Sub-Category</label>
                <select class="form-control" id="subcategory" name="subcategory" onChange={handleChange} value={values.subcategory}>
                    {
                        subCategory?
                        subCategory.map((cat2, index) => {
                            return <option>{cat2.name}</option>
                        }):<option>Others</option>
                    }
                </select>
            </div>
            <br />
            <div class="form-group">
                <label htmlFor="condition">Condition</label>
                <select class="form-control" id="condition" name="condition" onChange={handleChange} value={values.condition}>
                    <option value="" disabled selected hidden>Please choose the condition...</option>
                    <option>Brand New</option>
                    <option>Like New</option>
                    <option>Very Good</option>
                    <option>Good</option>
                    <option>Acceptable</option>
                </select>
            </div>
            <br />
            <div class="form-group">
                <label htmlFor="itemdescription">Item description</label>
                <textarea class="form-control" rows="5" id="itemdescription" name="itemdescription" onChange={handleChange} value={values.itemdescription}></textarea>
            </div>
            <br />
            <div class="form-group">
                <label htmlFor="images">Images</label>
                <div>
                    <input type="file" onChange={handleImageChange} />
                    <div class="row">
                        {imageList.map(x => {
                            return (<p><img src={x} alt="Image" width="50%" height="170" /></p>)
                        })}
                    </div>
                </div>
            </div>
            <br />

            <div className="text-right">
                <button className="btn btn-primary" onClick={handleNext}>Continue</button>
            </div>
        </div >
    )
}

export default ListingDetails