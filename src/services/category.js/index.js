import { API } from "../httpHandler/index";
import { getToken, setToken } from "../storageUtil";

export const getCategory = async () => {
    try {
        const data = await API.get(
            "http://localhost:5050/category"
        );
        return {
            data: data.data,
        };
    } catch (err) {
        return {
            error: err
        }
    }
}

export const getSubCategory = async (id) => {
    try {
        const data = await API.get(
            "http://localhost:5050/category/subCatergories", { params: { id } }
        );
        return {
            data: data.data,
        };
    } catch (err) {
        return {
            error: err
        }
    }
}

export const getSubCategoryByName = async (id) => {
    try {
        const data = await API.get(
            "http://localhost:5050/category/name", { params: { id } }
        );
        return {
            data: data.data,
        };
    } catch (err) {
        return {
            error: err
        }
    }
}

export const getCategoryById = async (id) => {
    try {
        const data = await API.get(
            "http://localhost:5050/category/getById", { params: { id } }
        );
        return {
            data: data.data,
        };
    } catch (err) {
        return {
            error: err
        }
    }
}
