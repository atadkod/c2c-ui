import React, { useRef } from 'react';
import Modal from '../../components/Modal';
import Navbar from '../HomePage/NavBar';
import BidPrice from './BidPrice';
import Card from './Card';
import './Product.css'
import ReactImageMagnify from 'react-image-magnify';
import { useEffect, useState } from 'react';
import { getProductDetails } from "../../services/products";
import { getBidsByProductId } from '../../services/Bid';

const Product = () => {
    console.log(window.location.pathname.split("/")[1])
    const [product, setProduct] = useState([])
    const [bids, setBids] = useState([])
    let count=1;
    useEffect(() => {
        (async () => {
            const uuid = window.location.pathname.split("/")[2];
            const data = await Promise.all([getProductDetails(uuid) ,getBidsByProductId(uuid)]);
            setProduct(data[0].data);
            setBids(data[1].data);
        })()
    }, [])

    const inputEl = useRef(null);
    console.log(product,bids)
    const bidding = <><div>Price:</div>
        <input type="text" />
        <input type="text" />
        <div onClick={() => { console.log("hi") }}>Submit</div></>

    const watchlisting = <><div>Do you want to add this product to your watchlist!!!</div>
        <br />
        <div onClick={() => { console.log("hi") }}>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div></>

    return (
        <div>
            <div className="ProductBg">
                <Navbar />
                <div className="container-fluid p-2"></div>
                <div className="container ProductDesc">
                    <div className="row">
                        <div className="col-md-1"></div>
                        <div className="col-md-4 ProductImage">
                            <br /><br /><br /><br /><br />
                            <div className="perimeter" onMouseEnter={() => inputEl.current.style.opacity = '0'} onMouseLeave={() => inputEl.current.style.opacity = '1'}>
                                <div className="image">
                                    <ReactImageMagnify {...{
                                        smallImage: {
                                            alt: 'Wristwatch by Ted Baker London',
                                            isFluidWidth: true,
                                            src: "https://static.lenovo.com/na/landing-pages/oculus-rift/lenovo-oculus-hero-smartphone.png",
                                            sizes: '(min-width: 800px) 33.5vw, (min-width: 415px) 100vw, 100vw',
                                        },
                                        largeImage: {
                                            alt: '',
                                            src: "https://static.lenovo.com/na/landing-pages/oculus-rift/lenovo-oculus-hero-smartphone.png",
                                            width: 1200,
                                            height: 800
                                        },
                                        isHintEnabled: true
                                    }} />
                                </div>


                            </div>
                            <br /><br />

                            <img style={{border:"1px solid black"}}height="100" width="100" src={"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRI23pK8CXhHQozXxn-p6r9lNtkzkQzH0vMRw&usqp=CAU"} /> &nbsp;

                            <img  style={{border:"1px solid black"}}height="100" width="100" src={"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSZE8yUmGpy67jusEixTYaEixNxeKW0S6sfPQ&usqp=CAU"} /> &nbsp;

                            <img style={{border:"1px solid black"}} height="100" width="100" src={"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSVN-Wk2PvZ_QThoAmKzKFqI35tTNt-Sc0EZQ&usqp=CAU"} /> &nbsp;


                        </div>
                        <div className="col-md-6 ProductBids" ref={inputEl}>
                            <div className="ProductName text-center"> {product.name}<br /></div>

                            <br /><br />

                            <Card product={product} />

                            <div className="listofbids container card shadow p-5 mb-5 bg-body rounded">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Bid</th>
                                            <th scope="col">Date</th>
                                        </tr>
                                    </thead>
                                    {bids&&bids.map((bid) => {
                                        return (
                                        bid&&
                                        <tbody>
                                            <tr className="text-success fw-bold">
                                                <td>{ count++ }</td>
                                                <td>$ {bid.bidPrice}</td>
                                                <td>{bid.updatedAt}</td>
                                            </tr>
                                        </tbody>
                                        );
                                    })}
                                </table>
                                {!bids?<i>No bids for the product are available</i>:<i>Above bids are arranged in descending orders of Price and Date</i>}
                            </div>
                           
                        </div>
                       </div>
                    </div>
                </div>

                {/* Here, Seller Name, Price, Date, Rating, Option to wishlist or bid, Option to update bid, Option to Pay, OPtion to view Progress*/}
                

                <Modal modalName="bid" title="Submit your bid" content={<BidPrice />} />

                <Modal modalName="watchlist" title="WatchList the product" content={watchlisting} />

            </div>
    );
};

export default Product