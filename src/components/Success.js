import React, { Component } from 'react'

export class Success extends Component {
    render() {
        const handleConfirmDialog = () => {
            this.props.closeButton.click()
        }
        console.log(this.props.closeButton)
        return (
            <div>
                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '15' }}>
                    <img src="Done.png" width="80" height="80" /><br />

                </div>
                <div>
                    <h1 className="text-black">Product Added Successfully!</h1>
                </div>

                <button type="button" class="btn btn-blue" style={{ alignItems: 'center' }} onClick={handleConfirmDialog}>
                    <a >
                        Continue
                    </a>
                </button>
            </div>

        )
    }
}

export default Success