import React from 'react';

function Card({ product }) {
    return (
        <div>
            {product ? <div class="card text-dark bg-light m-5" style={{ borderRadius: "15px" }}>
                <div class="card-body p-5">
                    <div className="row">
                        <div className="col-md-3">
                            <div class="" style={{ margin: '0', position: 'absolute', top: '50%', msTransform: 'translateY(-50%)', transform: 'translateY(-50%)' }}>
                                <span className="fw-bold" style={{ fontSize: "30px" }}>{product.name}</span>
                                <br />

                            </div>
                        </div>

                        <div className="col-md-2">
                            <div class="">
                                <span className="fw-bold text-success" style={{ fontSize: "30px" }}>${product.price}</span>
                                <br />
                                Starting bid
                            </div>
                        </div>
                        <div className="col-md-2">
                            <div class="">
                                <span className="fw-bold text-success" style={{ fontSize: "30px" }}>{product.threshold?product.threshold:product.bidPrice}$</span>
                                <br />
                                {product.threshold?
                                <span>Threshold Amount</span>
                                :<span>Bid Amount</span>
                                }
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div class="">
                                <span className="fw-bold text-success" style={{ fontSize: "30px" }}>{product.highestPrice}$</span>
                                <br />
                                Highest
                            </div>
                        </div>

                        <div className="col-md-2">
                            <button type="button" class="btn btn-dark" onClick={() => window.location.href = "/product"} style={{ margin: '0', position: 'absolute', top: '50%', msTransform: 'translateY(-50%)', transform: 'translateY(-50%)' }}>Click to view</button>
                        </div>
                    </div>
                </div>
            </div> : null
            }
        </div>
    );
}

export default Card;