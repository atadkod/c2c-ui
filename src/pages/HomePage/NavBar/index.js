import React, { useState } from 'react';
import Login from '../Login';
import '../homepage.css'
import Form from "../../../components/Form"
import { useNavigate } from "react-router";
import { getToken, deleteToken } from '../../../services/storageUtil';
import Modal from '../../../components/Modal';
import { Dropdown } from 'react-bootstrap';

const Navbar = () => {
    const navigate = useNavigate();
    const logoutUser = async () => {
        deleteToken()
        window.location.href = "/"
    }
    const sellersView = async () => {
        navigate("/seller")
    }

    const buyersView = async () => {
        navigate("/customer")
    }

    const [closeLoginBtn, setCloseloginBtn] = useState(null)
    const [closeBtn, setCloseBtn] = useState(null)
    const token = getToken()
    return (
        <div>
            <div className="container-fluid" style={{ height: "", backgroundColor: "#e1e4e6" }}>
                <div className="container p-5">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <div class="container-fluid">
                            <a class="navbar-brand" href="/"><img src="logo.png" /></a>
                            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav mx-auto ">
                                    <li class="nav-item">
                                        <a class="nav-link " aria-current="page" href="/#homeAuction">My orders</a>
                                    </li>&nbsp;&nbsp;&nbsp;
                                    <li class="nav-item">
                                        <div className="dropdown">
                                            <Dropdown>
                                                <Dropdown.Toggle
                                                    variant="secondary"
                                                    id="dropdown-basic" style={{ backgroundColor: 'transparent', borderColor: 'transparent', color: 'rgba(0,0,0,.55)', fontWeight: 'inherit' }}>
                                                    DashBoard
                                                </Dropdown.Toggle>

                                                <Dropdown.Menu style={{ backgroundColor: '#e1e4e6' }}>
                                                    <Dropdown.Item onClick={sellersView} >Sellers view</Dropdown.Item>
                                                    <Dropdown.Item onClick={buyersView}>Buyers view</Dropdown.Item>
                                                </Dropdown.Menu>
                                            </Dropdown>
                                        </div>
                                    </li>&nbsp;&nbsp;&nbsp;
                                    <li class="nav-item">
                                        <a class="nav-link " aria-current="page" href="/#homeAuction">Bid a product</a>
                                    </li>&nbsp;&nbsp;&nbsp;
                                    <li class="nav-item">
                                        <a class="nav-link" role="button" data-bs-toggle="dropdown" aria-expanded="false" data-bs-toggle="modal" data-bs-target="#productSellRegistration" >Sell a product</a>
                                    </li>&nbsp;&nbsp;&nbsp;
                                    <li class="nav-item dropdown">
                                        {!token ? <a class="btn btn-dark" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                            Login
                                        </a> : <a class="btn btn-dark" id="navbarDropdown" role="button" onClick={logoutUser}>
                                            Logout
                                        </a>}
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </nav>
                    <Modal modalName="exampleModal" title="Login" setCloseBtn={setCloseBtn} content={<Login closeButton={closeBtn} />} />
                    <Modal modalName="productSellRegistration" setCloseBtn={setCloseloginBtn} title="Product Registration" content={<Form closeButton={closeLoginBtn} />} />

                </div>
            </div>
        </div>
    );
};

export default Navbar;