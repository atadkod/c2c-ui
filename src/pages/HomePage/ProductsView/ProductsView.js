import React, { useEffect, useState } from 'react';
import { getAllProducts } from '../../../services/products';
import Card from '../Card';
import Categories from '../Categories';
import './ProductsView.css';


const ProductsView = () => {
    const [ProductsList, setProductList] = useState([])
    const [cat, setCat] = useState({ name: "All" })

    useEffect(() => {
        (async () => {
            const res = await getAllProducts();
            setProductList(res.data)
        })()
    }, [])

    const handleCategory = (c) => {
        setCat(c)
        console.log(c)
    }

    return (
        <div>

            <Categories handleCategory={handleCategory} />
            <div class="container-fluid productsView p-5">

                <div class="row">
                    <div class="col-md-8">
                        <h1 class="display-4 fw-bold">{cat.name}</h1><br />
                    </div>
                    <div class="col-md-4">
                        <input className="textline" placeholder="Search your favourite Brands" type="text" /> &nbsp;<i class="fa fa-search"></i>
                    </div>
                </div>



                <div class="row">
                    <div class="col-md-2 Brands">
                        <div class="container p-5">
                            <h2>Brands</h2>
                            <form>

                                <div class="checkbox">
                                    <label><input type="checkbox" value="" />&nbsp;&nbsp;All</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" value="" />&nbsp;&nbsp;LG</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" value="" />&nbsp;&nbsp;Whirlpool</label>
                                </div>
                                <div class="checkbox disabled">
                                    <label><input type="checkbox" value="" />&nbsp;&nbsp;Bosch</label>
                                </div>


                            </form>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="vl"></div>
                    </div>

                    <div class="col-md-9">
                        <div className="bg-light">
                            <div class="container">

                                {ProductsList ? ProductsList.map((Products) =>
                                    <div className="row">
                                        {Products.map((p) =>
                                            <div className="col-md-4"><Card product={p} /></div>
                                        )}
                                    </div>
                                )
                                    : (<div class="spinner-border" role="status">
                                        <span class="visually-hidden">Loading...</span>
                                    </div>)}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ProductsView;