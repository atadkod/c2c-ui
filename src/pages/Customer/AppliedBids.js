import React, { useEffect, useState } from 'react';
import { getAppliedBids, getHighestBid } from '../../services/Bid';
import { getProductDetails } from '../../services/products';
import Card from './Card';


const AppliedBids = () => {
    const [productList, setProductList] = useState([])

    useEffect(() => {
        (async () => {
            const res = await getAppliedBids();
            const newRes = []
            if (res.data) {
                for (const r of res.data) {
                    const prod = await getProductDetails(r.productId)
                    const highest = await getHighestBid(r.productId)
                    newRes.push({ ...r, ...prod.data, highestPrice: highest.data.bidPrice })
                }
                setProductList(newRes)
            }
        })()
    }, [])
    return (productList ?
        <div>
            {productList.map(product => {
                return < Card product={product} />
            })
            }
        </div> : null
    );
};

export default AppliedBids;