import React from "react";
import { useForm } from "react-hook-form";
import { useLocation, useNavigate } from 'react-router-dom'
import '../../pages/HomePage/homepage.css'
import { bidPrice } from "../../services/Bid";

export default function BidPrice({ closeButton, product }) {
    const { register, handleSubmit } = useForm();
    const navigate = useNavigate();
    const location = useLocation();
    const onSubmit = async (data) => {
        console.log(data)
        const result = await bidPrice(data, product)
        if (result.error) {
            console.log(result.error)
        } else {
            navigate(location.pathname)
            closeButton.click()
        }

    };

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <div class="mb-3">
                <label for="basPrice" class="form-label" >Base Price: {product?product.price:null}</label>
            </div>

            <div class="mb-3">
                <label for="bidPrice" class="form-label" >Price</label>
                <input type="number" class="form-control" {...register("price")} id="bidPrice" />
            </div>
            <div class="mb-3">
                <label for="message" class="form-label" >Add a comment</label>
                <textArea row="3" class="form-control" {...register("message")} id="message" />
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>

        </form >


    );
}