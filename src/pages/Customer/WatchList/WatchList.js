import React, { useEffect, useState } from 'react';
import Card from '../Card';
import { getWishListByBuyerId } from "../../../services/watchlist";
import { getProductDetails } from "../../../services/products";
import { getHighestBid } from "../../../services/Bid";
const WatchList = () => {
    const [Items, setItems] = useState([])
    useEffect(() => {
        (async () => {
            const res = await getWishListByBuyerId();
            console.log("@@@@", res);

            const newRes = []
            if (res.data) {
                for (const r of res.data) {
                    const prod = await getProductDetails(r.productId)
                    const highest = await getHighestBid(r.productId)
                    newRes.push({ ...r, ...prod.data, highestPrice: highest.data.bidPrice })
                }
                setItems(newRes)
            }
        })()
    }, [])
    return (Items ?
        <div>
            {Items.map(item => {
                return < Card product={item} />
            })
            }
        </div> : null
    );
};

export default WatchList;