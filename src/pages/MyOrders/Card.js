import React from 'react';
import Modal from '../../components/Modal';

function Card({ product }) {
    const watchlisting = <><div>Do you want to add this product to your watchlist!!!</div>
        <br />
        <div onClick={() => { console.log("hi") }}>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div></>
    return (
        <div>
            {product ? <div class="card text-dark bg-light m-5" style={{ borderRadius: "15px" }}>
                <div class="card-body p-5">
                    <div className="row">
                        <div className="col-md-2">
                            <div class="" style={{ margin: '0', position: 'absolute', top: '50%', msTransform: 'translateY(-50%)', transform: 'translateY(-50%)' }}>
                                <span className="fw-bold" style={{ fontSize: "30px" }}>{product.name}</span>
                                <br />

                            </div>
                        </div>
                        <div className="col-md-2">
                            <div class="">
                                <span className="fw-bold text-success" style={{ fontSize: "30px" }}>{product.category}</span>
                                <br />
                                Category
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div class="">
                                <span className="fw-bold text-success" style={{ fontSize: "30px" }}>${product.price}</span>
                                <br />
                                Starting bid
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div class="">
                                <span className="fw-bold text-success" style={{ fontSize: "30px" }}>${product.purchasedprice}</span>
                                <br />
                                Purchased Price 
                            </div>
                        </div>

                        <div className="col-md-2">
                            <div class="">
                                <span className="fw-bold text-success" style={{ fontSize: "30px" }}>{product.duration}</span>
                                <br />
                                Time Left
                            </div>
                        </div>

                        <div className="col-md-2">
                            <button type="button" class="btn btn-dark" onClick={() => window.location.href = "/product"} style={{ margin: '0', position: 'absolute', top: '50%', msTransform: 'translateY(-50%)', transform: 'translateY(-50%)' }}>Click to view</button>
                        </div>
                    </div>
                </div>
            </div> : null
            }

            <Modal modalName="bid" title="Submit your bid" content={<BidPrice />} />

            <Modal modalName="watchlist" title="WatchList the product" content={watchlisting} />
        </div>
    );
}

export default Card;