import React, { Component } from 'react';
import SellingDetails from './SellingDetails';
import Confirm from './Confirm';
import Success from './Success';
import ListingDetails from './ListingDetails';
import { postProduct } from '../services/products';

export class Form extends Component {
    state = {
        step: 1,
        title: '',
        category: '',
        subcategory: '',
        condition: '',
        itemdescription: '',
        images: '',
        startdate: '',
        enddate: '',
        price: '',
        quantity: '',
        paymentmethod: '',
        loading: true
    };

    nextStep = () => {
        const { step } = this.state;
        this.setState({ step: step + 1 });
    };

    prevStep = () => {
        const { step } = this.state;
        this.setState({ step: step - 1 });
    };

    inputChange = (input, e) => {
        this.setState({
            [input]: e.target.value
        });
    };
    submitForm = async () => {
        const { title, category, subcategory, condition, itemdescription, images, startdate, enddate, price, quantity, paymentmethod } = this.state;
        const values = { title, category, subcategory, condition, itemdescription, images, startdate, enddate, price, quantity, paymentmethod };
        const res = await postProduct(values)
        console.log(res)
        this.setState({ loading: false })
        this.nextStep()
    }
    render() {

        console.log(this.props)
        const { step } = this.state;
        const { title, category, subcategory, condition, itemdescription, images, startdate, enddate, price, quantity, paymentmethod } = this.state;
        const values = { title, category, subcategory, condition, itemdescription, images, startdate, enddate, price, quantity, paymentmethod };

        switch (step) {
            case 1:
                return (
                    <ListingDetails
                        nextStep={this.nextStep}
                        inputChange={this.inputChange}
                        values={values}
                    />
                );
            case 2:
                return (
                    <SellingDetails
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        inputChange={this.inputChange}
                        values={values}
                    />
                );
            case 3:
                return (
                    <Confirm
                        nextStep={this.nextStep}
                        prevStep={this.prevStep}
                        postData={this.submitForm}
                        values={values}
                    />
                );
            case 4:
                return !this.state.loading ? (
                    <Success closeButton={this.props.closeButton} />
                ) : null;
        }
    }
}

export default Form