import React, { Component } from 'react'

export class SellingDetails extends Component {
    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    };

    back = e => {
        e.preventDefault();
        this.props.prevStep();
    };

    render() {
        const handleChange = (e) => {
            inputChange(e.target.name, e)
        }
        const { values, inputChange } = this.props;

        return (
            <div className="form-container">
                <h1 className="mb-5">Selling details</h1>
                <div className="form-group">
                    <label htmlFor="startdate">Start Date</label>
                    <input type="date" className="form-control" id="startdate" name="startdate" onChange={handleChange} value={values.startdate} />
                </div>
                <br />
                <div className="form-group">
                    <label htmlFor="enddate">End Date</label>
                    <input type="date" className="form-control" id="enddate" name="enddate" onChange={handleChange} value={values.enddate} />
                </div>
                <br />
                <div className="form-group">
                    <label htmlFor="price">Price</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">₹</span>
                        </div>
                        <input type="number" class="form-control" id="price" name="price" onChange={handleChange} value={values.price} placeholder="0.00" />
                    </div>
                </div>

                <br />
                <div className="form-group">
                    <label htmlFor="quantity">Quantity</label>
                    <input type="number" className="form-control" id="quantity" name="quantity" onChange={handleChange} value={values.quantity} />
                </div>

                <br />
                <div className="form-group">
                    <label htmlFor="paymentmethod">Payment Method</label>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="paymentmethod" name="paymentmethod" onChange={handleChange} value={values.paymentmethod} disabled checked />
                        <label class="form-check-label" for="defaultCheck2">
                            Cash on delivery
                        </label>
                    </div>
                </div>
                <br />

                <div className="row">
                    <div className="col-6">
                        <button className="btn btn-danger" onClick={this.back}>Back</button>
                    </div>
                    <div className="col-6 text-right">
                        <button className="btn btn-primary" onClick={this.continue}>Continue</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default SellingDetails