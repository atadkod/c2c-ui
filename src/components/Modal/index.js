import React from 'react';

const Modal = (props) => {
    console.log(props.modalName)
    return (
        <div>
            <div className="modal fade" id={props.modalName} tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h2 className="modal-title" id="exampleModalLabel">{props.title}</h2>
                            <button ref={btn => props.setCloseBtn ? props.setCloseBtn(btn) : null} type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            {props.content}


                        </div>

                    </div>
                </div>

            </div>
        </div>
    );
};

export default Modal;